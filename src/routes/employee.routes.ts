import { Router } from 'express'
import EmployeeController from '../controllers/EmployeeController'

const employeeRouter = Router()
const employeeController = new EmployeeController()


employeeRouter.post("/", employeeController.create)
 
employeeRouter.get("/", employeeController.list)

employeeRouter.get("/:id", employeeController.getById)

employeeRouter.get("/findby/name", employeeController.getByName)

employeeRouter.get("/cpf/cpf", employeeController.getByCpf)

employeeRouter.delete("/:id", employeeController.delete)

employeeRouter.put("/:id", employeeController.update)


export default employeeRouter