import { Router } from 'express'
import SpotValueController from '../controllers/SpotValueController'

const spotValueRouter = Router()
const spotValueController = new SpotValueController()


spotValueRouter.post("/", spotValueController.create)
 
spotValueRouter.get("/", spotValueController.list)

spotValueRouter.get("/:id", spotValueController.getById)

spotValueRouter.delete("/:id", spotValueController.delete)

spotValueRouter.put("/:id", spotValueController.update)


export default spotValueRouter