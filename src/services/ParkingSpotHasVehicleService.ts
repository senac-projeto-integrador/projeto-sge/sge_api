import { ParkingSpotHasVehicleRepository } from '../repositories/ParkingSpotHasVehicleRepository'
import { ParkingSpotRepository } from '../repositories/ParkingSpotRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'
import ParkingSpot from '../models/ParkingSpot'
import Vehicle from '../models/Vehicle'
import Client from '../models/Client'
import timeCalc from '../utils/timeCalc'

export interface Request {
    parkingSpotId: ParkingSpot,
    vehicleId: Vehicle,
    clientId: Client,
    checkOut: Date | null,
    checkIn: Date,
    totalValue: number | null,
    status: boolean,
}

export default class ParkingSpotHasVehicleService {

    async create({ parkingSpotId, vehicleId, clientId, status }: Request) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const spotValue = await parkingSpotRepository.findOne(parkingSpotId)

        const parkingSpotValue = spotValue ? spotValue.spotValueId.value : 0

        if (parkingSpotValue === 0) {
            throw new AppError('Id não encontrado')
        }

        const parkingSpotHasVehicleCreate = parkingSpotHasVehicleRepository.create({
            parkingSpotId,
            vehicleId,
            clientId,
            checkIn: new Date(),
            status,
        })

        await parkingSpotHasVehicleRepository.save(parkingSpotHasVehicleCreate)
        return parkingSpotHasVehicleCreate
    }

    async list() {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const all = await parkingSpotHasVehicleRepository.find({ 
            relations: ["parkingSpotId", "clientId", "vehicleId"], 
            order: {id: "ASC"},
        })

        const filtro = all.filter(items => { 
            return items.checkOut === null
        })

        return filtro
    }

    async getById(id: number) {

        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const findeOne = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "clientId", "vehicleId"] })

        if (!findeOne) {
            throw new AppError('Id não encontrado')
        }

        return findeOne
    }

    async Update(id: number, { parkingSpotId, vehicleId, clientId, checkIn, status }: Request) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const findParkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id)

        if (!findParkingSpotHasVehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        await parkingSpotHasVehicleRepository.update(id, {
            parkingSpotId,
            vehicleId,
            clientId,
            checkIn,
            status,
        })

        const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleRepository.findOne(id)

        return parkingSpotHasVehicleUpdate
    }

    async delete(id: number) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id)

        if (!parkingSpotHasVehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const deleted = await parkingSpotHasVehicleRepository.delete(id)

        return deleted
    }

    async leave(id: number) {
        const parkingSpotHasVehicleRepository = getCustomRepository(ParkingSpotHasVehicleRepository)

        const parkingSpotHasVehicle = await parkingSpotHasVehicleRepository.findOne(id, { relations: ["parkingSpotId", "clientId", "vehicleId"] })


        if (!parkingSpotHasVehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const checkIn = parkingSpotHasVehicle.checkIn
        const timeValue = parkingSpotHasVehicle.parkingSpotId.spotValueId.value

        const checkOut = new Date()

        const totalValue = timeCalc(checkIn, checkOut, timeValue)

        console.log(totalValue)


        parkingSpotHasVehicleRepository.update(id, {
            checkOut,
            totalValue,
        })

        return { totalValue }
    }

} 


