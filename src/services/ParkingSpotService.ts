import { ParkingSpotRepository } from '../repositories/ParkingSpotRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'
import SpotValue from '../models/SpotValue'

export interface Request {
    name: string
    spotValueId: SpotValue
}

export default class ParkingSpotService {

    async create({ name, spotValueId }: Request) {

        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = parkingSpotRepository.create({
            name,
            spotValueId
        })

        await parkingSpotRepository.save(parkingSpot)
        return parkingSpot
    }

    async list() {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const all = await parkingSpotRepository.find({ relations: ["spotValueId"]})

        return all
    }

    async getById(id: number) {

        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = await parkingSpotRepository.findOne(id, {relations: ["spotValueId"]})

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        return parkingSpot
    }

    async delete(id: number) {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = await parkingSpotRepository.findOne(id)

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        const deleted = await parkingSpotRepository.delete(id)

        return deleted
    }

    async Update(id: number, { name, spotValueId }: Request) {
        const parkingSpotRepository = getCustomRepository(ParkingSpotRepository)

        const parkingSpot = await parkingSpotRepository.findOne(id)

        if (!parkingSpot) {
            throw new AppError('Vaga não encontrada!')
        }

        await parkingSpotRepository.update(id, {
            name,
            spotValueId
        })

        const parkingSpotUpdate = await parkingSpotRepository.findOne(id)

        return parkingSpotUpdate
    }

}

