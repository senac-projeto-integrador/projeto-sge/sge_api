import { Request, Response } from "express"

import ClientService from '../services/ClientService'
const clientService = new ClientService()

export default class ClientController {
    async create(req: Request, res: Response) {
        try {
            const client = await clientService.create(req.body)
            return res.status(201).json(client)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await clientService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getByName(req: Request, res: Response) {
        try {
            const client = await clientService.getByName(req.body.name)
            return res.status(200).json(client)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const client = await clientService.getById(Number(req.params.id))
            return res.status(200).json(client)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await clientService.delete(Number(req.params.id))
            return res.status(200).json('Cliente excluido com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const clientUpdate = await clientService.Update(Number(req.params.id), req.body)
            return res.status(200).json(clientUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
