import { Request, Response } from "express"

import EmployeeService from '../services/EmployeeService'
const employeeService = new EmployeeService()

export default class EmployeeController {
    async create(req: Request, res: Response) {
        try {
            const employee = await employeeService.create(req.body)
            return res.status(201).json(employee)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await employeeService.list()
            return res.status(200).json(listAll)       
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const employee = await employeeService.getById(Number(req.params.id))
            return res.status(200).json(employee)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
   
    async getByName(req: Request, res: Response) {
        try {
            const employee = await employeeService.getByName(req.body.name)
            return res.status(200).json(employee)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getByCpf(req: Request, res: Response) {
        try {
            const employee = await employeeService.getByCpf(req.body.cpf)
            return res.status(200).json(employee)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await employeeService.delete(Number(req.params.id))
            return res.status(200).json('Funcionário excluido com sucesso!')            
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const employeeUpdate = await employeeService.Update(Number(req.params.id), req.body)
            return res.status(200).json(employeeUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
