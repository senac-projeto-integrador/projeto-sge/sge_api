import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity("amountSpot")
export default class AmountSpot {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'amount_spot' })
    amountSpot: number

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}