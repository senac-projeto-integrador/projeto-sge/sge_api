import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import Client from "./Client";
import Vehicle from "./Vehicle";
import ParkingSpot from "./ParkingSpot";

@Entity("parkingSpotHasVehicle")
export default class ParkingSpotHasVehicle {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => ParkingSpot, { nullable: true })
    @JoinColumn({ name: 'parkingSpot_id' })
    parkingSpotId: ParkingSpot;
    
    @ManyToOne(() => Vehicle, { nullable: true })
    @JoinColumn({ name: 'vehicle_id' })
    vehicleId: Vehicle;
    
    @ManyToOne(() => Client, { nullable: true })
    @JoinColumn({ name: 'client_id' })
    clientId: Client;

    @Column({ nullable: true })
    checkOut: Date

    @Column()
    checkIn: Date

    @Column({ name: "total_value", nullable: true })
    totalValue: number

    @Column()
    status: boolean

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}