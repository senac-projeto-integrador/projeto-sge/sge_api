import { EntityRepository, Repository } from "typeorm";
import AmountSpot from "../models/AmountSpot"

@EntityRepository(AmountSpot) 
class AmountSpotRepository extends Repository<AmountSpot> {

}
export{AmountSpotRepository}